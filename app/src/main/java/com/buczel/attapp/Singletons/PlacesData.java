package com.buczel.attapp.Singletons;

/**
 * Created by Reiz3N on 2016-05-03.
 */
public class PlacesData {

    private static PlacesData instance;

    private Data[] data  = new Data[4];

    //no outer class can initialize this class's object
    private PlacesData() {

    }

    public static PlacesData Instance()
    {
        //if no instance is initialized yet then create new instance
        //else return stored instance
        if (instance == null)
        {
            instance = new PlacesData();
        }
        return instance;
    }

    public void insertDataDetails(int index, int popularity, float rating, String name){
        if(data[index]==null) data[index] = new Data();

        data[index].setPopularity(popularity);
        data[index].setRating(rating);
        data[index].setName(name);
    }

    public void insertDataDistance(int index, String distanceText, int distanceValue){
        if(data[index]==null) data[index] = new Data();

        data[index].setDistanceText(distanceText);
        data[index].setDistanceValue(distanceValue);
    }

    public Data[] getData() {
        return data;
    }

    public void setData(Data[] data) {
        this.data = data;
    }


    public class Data{
        String distanceText;
        int distanceValue;
        int popularity;
        float rating;
        String name;

        public String getDistanceText() {
            return distanceText;
        }

        public void setDistanceText(String distanceText) {
            this.distanceText = distanceText;
        }

        public int getDistanceValue() {
            return distanceValue;
        }

        public void setDistanceValue(int distanceValue) {
            this.distanceValue = distanceValue;
        }

        public int getPopularity() {
            return popularity;
        }

        public void setPopularity(int popularity) {
            this.popularity = popularity;
        }

        public float getRating() {
            return rating;
        }

        public void setRating(float rating) {
            this.rating = rating;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }


}
