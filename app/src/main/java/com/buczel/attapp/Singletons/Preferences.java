package com.buczel.attapp.Singletons;

/**
 * Created by Reiz3N on 2016-05-02.
 */
public class Preferences implements MatrixNames {

    private static Preferences instance;

    //type of searched places
    private String placeType = "coffee";

    //which matrix we are currently editing
    private int matrixType;

    //size of matrixes, 4 because we are comparing 4 places
    private int nSize = 4;

    //main matrix containing all data delivered by user
    //#TODO Change this array to some other struct or class
    private int middleOption[][][] = new int[nSize][nSize][nSize];

    private double critMat[][] = new double[3][3];
    private double distMat[][] = new double[4][4];
    private double popuMat[][] = new double[4][4];
    private double rateMat[][] = new double[4][4];

    private int selectedLeft, selectedRight;

    private boolean isRightSelected;

    //no outer class can initialize this class's object
    private Preferences() {
        //initialize main data matrix with -1;
        clearAll();
    }

    public static Preferences Instance() {
        //if no instance is initialized yet then create new instance
        //else return stored instance
        if (instance == null) {
            instance = new Preferences();
        }
        return instance;
    }

    public void clearAll(){
        //initialize main data matrix with -1;
        for (int i = 0; i < nSize; i++)
            for (int j = 0; j < nSize; j++) {
                distMat[i][j]=-1;
                popuMat[i][j]=-1;
                rateMat[i][j]=-1;
                for (int k = 0; k < nSize; k++)
                    middleOption[i][j][k] = -1;
            }

        for (int i = 0; i < 3; i++)
            for (int j = 0; j < 3; j++)
                critMat[i][j]=-1;
    }

    public int getMiddleOption() {
        //you must reduce one, becouse program (list) starts from 0 while AHP starts from 1
        return middleOption[matrixType][selectedLeft][selectedRight] - 1;
    }

    public void setMiddleOption(int selectedOption) {
        //you must add one, becouse program (list) starts from 0 while AHP starts from 1
        middleOption[matrixType][selectedLeft][selectedRight] = selectedOption + 1;
    }

    public int getSelectedLeft() {
        return selectedLeft;
    }

    public void setSelectedLeft(int selectedLeft) {
        isRightSelected = false;
        this.selectedLeft = selectedLeft;
    }

    public int getSelectedRight() {
        return selectedRight;
    }

    public void setSelectedRight(int selectedRight) {
        isRightSelected = true;
        this.selectedRight = selectedRight;
    }

    private void createMatrix(int size, int type) {

        double[][] tempMat = new double[size][size];

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (i == j) tempMat[i][j] = 1;

                if (i == 0 && j < size - 1) tempMat[i][j + 1] = middleOption[type][i][j];

                if (i == 1 && j < size - 2) tempMat[i][j + 2] = middleOption[type][i][j];

                if (i == 2 && j < size - 3) tempMat[i][j + 3] = middleOption[type][i][j];
            }
        }

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {

                if (i == 1 && j < 1) tempMat[i][j] = 1 / tempMat[j][i];

                if (i == 2 && j < 2) tempMat[i][j] = 1 / tempMat[j][i];

                if (i == 3 && j < 3) tempMat[i][j] = 1 / tempMat[j][i];
            }
        }

        switch (type) {
            case 0:
                critMat = tempMat;
                break;
            case 1:
                distMat = tempMat;
                break;
            case 2:
                popuMat = tempMat;
                break;
            case 3:
                rateMat = tempMat;
                break;
        }
    }

    public int getMatrixType() {
        return matrixType;
    }

    public void setMatrixType(int matrixType) {
        this.matrixType = matrixType;
    }

    public String getPlaceType() {
        return placeType;
    }

    public void setPlaceType(String placeType) {
        this.placeType = placeType;
    }

    public boolean isRightSelected() {
        return isRightSelected;
    }

    public double[][] getCritMat() {
        return critMat;
    }

    public double[][] getDistMat() {
        return distMat;
    }

    public double[][] getPopuMat() {
        return popuMat;
    }

    public double[][] getRateMat() {
        return rateMat;
    }

    public boolean checkDataGiven() {

        switch (matrixType) {
            case 0:
                createMatrix(3, matrixType);
                break;
            default:
                createMatrix(4, matrixType);
                break;
        }

        int x = 3, y = 3;
        if (matrixType != 0) {
            x = 4;
            y = 4;
        }

        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                switch (matrixType) {
                    case 0:
                        if (critMat[i][j] == -1) return false;
                        break;
                    case 1:
                        if (distMat[i][j] == -1) return false;
                        break;
                    case 2:
                        if (popuMat[i][j] == -1) return false;
                        break;
                    case 3:
                        if (rateMat[i][j] == -1) return false;
                        break;
                }


            }
        }
        return true;
    }
}

//:)
