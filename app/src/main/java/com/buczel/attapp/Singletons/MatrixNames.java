package com.buczel.attapp.Singletons;

/**
 * Created by Reiz3N on 2016-05-09.
 */
public interface MatrixNames {

    int CRITERIA = 0;
    int DISTANCE = 1;
    int POPULARITY = 2;
    int RATINGS = 3;
}
