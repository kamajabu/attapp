package com.buczel.attapp.AHPDataPreview;

import android.app.Activity;
import android.os.Bundle;
import android.text.InputType;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;

import com.buczel.attapp.Singletons.Preferences;
import com.buczel.attapp.R;

public class AHPActivity extends Activity {

    protected int a = 2, zupa = 10;
    TableLayout myTable, myTable2, myTable3, myTable4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ahp);



        myTable = (TableLayout) findViewById(R.id.tableLayout1);
        myTable2 = (TableLayout) findViewById(R.id.tableLayout2);
        myTable3 = (TableLayout) findViewById(R.id.tableLayout3);
        myTable4 = (TableLayout) findViewById(R.id.tableLayout4);

        fillTable(3, Preferences.Instance().getCritMat(), myTable);

        fillTable(4, Preferences.Instance().getDistMat(), myTable2);

        fillTable(4, Preferences.Instance().getPopuMat(), myTable3);

        fillTable(4, Preferences.Instance().getRateMat(), myTable4);

    }

    protected void fillTable(final int n, final double[][] matrix, TableLayout table) {
        table.removeAllViews();

        for (int i = 0; i < n; i++) {
            TableRow row = new TableRow(AHPActivity.this);
            row.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

            for (int j = 0; j < n; j++) {
                EditText edit = new EditText(AHPActivity.this);
                edit.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_DECIMAL|InputType.TYPE_NUMBER_FLAG_SIGNED);
                edit.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT));

                edit.setText(String.format("%.2f", matrix[i][j]));

                edit.setKeyListener(null);
                row.addView(edit);
            }
            table.addView(row);
        }
    }

}
