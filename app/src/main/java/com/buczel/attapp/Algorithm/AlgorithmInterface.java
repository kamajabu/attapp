package com.buczel.attapp.Algorithm;

/**
 * Created by Reiz3N on 2016-05-10.
 */
public interface AlgorithmInterface {

    //public double[] ahpAlgorithm(int preferencesMatrixSize, int criteriaMatrixesSize);
    public double[] getMatrix(int matrixType, int matrixSize, double[] matrix);
}
