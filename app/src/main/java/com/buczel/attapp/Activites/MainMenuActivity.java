package com.buczel.attapp.Activites;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.buczel.attapp.R;
import com.buczel.attapp.RetrofitAPI.FetchData;
import com.buczel.attapp.Singletons.Preferences;

public class MainMenuActivity extends Activity {

    private final String cafe = "cafe", dinner = "restaurant", drink = "bar";
    //protected GoogleApiClient mGoogleApiClient;
    private ImageButton buttonCoffee, buttonDinner, buttonDrink, buttonExit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        buttonCoffee = (ImageButton) findViewById(R.id.teaButton);
        buttonDinner = (ImageButton) findViewById(R.id.dinnerButton);
        buttonDrink = (ImageButton) findViewById(R.id.drinkButton);
        buttonExit = (ImageButton) findViewById(R.id.exitButton);


        buttonCoffee.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Preferences.Instance().setPlaceType(cafe);
                startNextActivity();
            }
        });

        buttonDinner.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Preferences.Instance().setPlaceType(dinner);
                startNextActivity();
            }
        });

        buttonDrink.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Preferences.Instance().setPlaceType(drink);
                startNextActivity();
            }
        });

        buttonExit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
                System.exit(0);
            }
        });
    }

    private void startNextActivity() {
        new FetchData(this);
        Intent i = new Intent(getBaseContext(), DecisionsMenuActivity.class); //DecisionsMenuActivity
        startActivity(i);

    }



}
