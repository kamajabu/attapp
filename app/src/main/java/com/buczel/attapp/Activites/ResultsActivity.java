package com.buczel.attapp.Activites;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.buczel.attapp.AHPDataPreview.AHPActivity;
import com.buczel.attapp.R;
import com.buczel.attapp.Singletons.PlacesData;
import com.buczel.attapp.Singletons.Preferences;
import com.witek.attapp.AHP;
import com.witek.attapp.Promethee;

import java.util.ArrayList;

public class ResultsActivity extends Activity {

    protected int indexMax = 0, indexMax2 = 0;
    private Button buttonShowMatrix, backButton;
    private TextView answer, answerLabel, placesNamesView, resultView;
    private PlacesData.Data[] placesData;
    private ArrayList<String> placesNames;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

        buttonShowMatrix = (Button) findViewById(R.id.showMatrix);
        backButton = (Button) findViewById(R.id.back);
        answer = (TextView) findViewById(R.id.answer);
        answerLabel = (TextView) findViewById(R.id.answerLabel);
        placesNamesView = (TextView) findViewById(R.id.placesNames);
        resultView = (TextView) findViewById(R.id.result);


        placesData = PlacesData.Instance().getData();
        placesNames = new ArrayList<>();

        buttonShowMatrix.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), AHPActivity.class);
                startActivity(i);
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Preferences.Instance().clearAll();

                Intent intent = new Intent(getBaseContext(), MainMenuActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

            }
        });

        AHP mAHP = new AHP();
        double rMatrix[] = mAHP.findAnswer();

        //answerLabel.setText("Answer:");
        String answerText = String.format("Wektor R decyzji: %.2f | %.2f | %.2f | %.2f \n\n", rMatrix[0], rMatrix[1], rMatrix[2], rMatrix[3]);
        answer.setText(answerText);

        indexMax = findMax(rMatrix);


        Promethee pro = new Promethee(mAHP.critMat, mAHP.distMat, mAHP.popuMat, mAHP.rateMat);
        double var[] = pro.calculateBestVariant();

        String answerTextPromethee = String.format("\nWektor współczynników algorytmu Promethee: %.2f | %.2f | %.2f | %.2f \n", var[0], var[1], var[2], var[3]);
        indexMax2 = findMax(var);
        answer.append(answerTextPromethee);

        new DownloadData().execute();


    }

    int findMax(double[] rMatrix) {
        int iMax = 0;
        double iValue = -1;
        for (int i = 0; i < rMatrix.length; i++) {
            if (rMatrix[i] > iValue) {
                iMax = i;
                iValue = rMatrix[i];
            }
        }
        return iMax;
    }


    public class DownloadData extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... arg0) {
            //new FetchData(ResultsActivity.this);

            while (placesData[3] == null) {
            }

            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            Toast.makeText(getApplicationContext(), "Dane pobrane.", Toast.LENGTH_SHORT).show();

            for (int i = 0; i < placesData.length; i++)
                placesNames.add(placesData[i].getName());

            for (String placeName : placesNames) {
                placesNamesView.append(String.format("%s\n", placeName));
            }

            String resultText = String.format("Najlepszym wyborem bedzie (AHP): %s\n\n", placesNames.get(indexMax));
            resultView.setText(resultText);

            String resultText2 = String.format("\nNajlepszym wyborem bedzie (Promethee): %s", placesNames.get(indexMax2));
            resultView.append(resultText2);

        }


    }
}
