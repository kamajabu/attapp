package com.buczel.attapp.Activites;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.buczel.attapp.Fragments.FragmentConnector;
import com.buczel.attapp.Fragments.LeftListFragment;
import com.buczel.attapp.Fragments.MiddleListFragment;
import com.buczel.attapp.Fragments.RightListFragment;
import com.buczel.attapp.R;
import com.buczel.attapp.Singletons.MatrixNames;
import com.buczel.attapp.Singletons.PlacesData;
import com.buczel.attapp.Singletons.Preferences;

import java.util.ArrayList;


public class DecisionsMenuActivity extends Activity implements FragmentConnector, MatrixNames {

    Button buttonNext, buttonPrevious;
    TextView stageName;
    PlacesData.Data[] placesData;

    boolean mockData = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        buttonNext = (Button) findViewById(R.id.buttonNext);
        buttonPrevious = (Button) findViewById(R.id.buttonPrevious);

        //we start on screen comparing criteria
        criteriaRate();

        //region buttonPrevious listener
        buttonPrevious.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                switch (Preferences.Instance().getMatrixType()) {
                    case CRITERIA:
                        finish();
                        break;
                    case DISTANCE:
                        criteriaRate();
                        break;
                    case POPULARITY:
                        distanceRate();
                        break;
                    case RATINGS:
                        popularityRate();
                        break;
                }
                clearMiddle();
            }
        });
        //endregion

        //region buttonNext listener
        buttonNext.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(Preferences.Instance().checkDataGiven()) {
                    switch (Preferences.Instance().getMatrixType()) {
                        case CRITERIA:
                            placesData = PlacesData.Instance().getData();
                            if (placesData[0] != null)
                                distanceRate();
                            else
                                Toast.makeText(getApplicationContext(), "Wciaz pobieram dane. Uwaga: Pamietaj, ze do dzialania " +
                                        "programu konieczne sa wlaczone uslugi lokalizacyjne!", Toast.LENGTH_LONG).show();
                            break;
                        case DISTANCE:
                            popularityRate();
                            break;
                        case POPULARITY:
                            ratingRate();
                            break;
                        case RATINGS:
                            //Toast.makeText(getApplicationContext(), "START AHP XD", Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(getBaseContext(), ResultsActivity.class);
                            startActivity(i);
                            break;
                    }
                    clearMiddle();
                } else
                    Toast.makeText(getApplicationContext(), "Nie wprowadziles wszystkich danych!", Toast.LENGTH_SHORT).show();

            }
        });
        //endregion

    }

    @Override
    public void onItemSelected(int selectedOption, listType type) {

        RightListFragment fragmentRight = (RightListFragment) getFragmentManager().findFragmentById(R.id.rightListFragment);
        MiddleListFragment fragmentMiddle = (MiddleListFragment) getFragmentManager().findFragmentById(R.id.middleListFragment);

        switch (type) {
            //if option on left fragment is selected (and/or changed)
            case LEFT:
                //save which one in singleton
                Preferences.Instance().setSelectedLeft(selectedOption);
                //update display on right fragment
                fragmentRight.updateList(selectedOption);
                //and clear currently selected option on middle
                fragmentMiddle.clearMiddle();
                break;
            //if option on middle fragment is selected
            case MIDDLE:
                //check if option from right fragment is selected
                if (Preferences.Instance().isRightSelected())
                    //if yes then save that option
                    Preferences.Instance().setMiddleOption(selectedOption);
                else {
                    //else clear selection and notice the user
                    fragmentMiddle.clearMiddle();
                    Toast.makeText(getApplicationContext(), "Najpierw musisz wybrac druga opcje!", Toast.LENGTH_SHORT).show();
                }
                break;
            case RIGHT:
                Preferences.Instance().setSelectedRight(selectedOption);
                fragmentMiddle.updateList();
                break;
            default:
                break;
        }


    }

    private void clearMiddle() {
        MiddleListFragment fragmentMiddle = (MiddleListFragment) getFragmentManager().findFragmentById(R.id.middleListFragment);
        fragmentMiddle.clearMiddle();
    }

    private void criteriaRate() {

        final ArrayList<String> criterias = new ArrayList<String>();
        criterias.add("Jakość");
        criterias.add("Odległość");
        criterias.add("Popularność");

        stageName = (TextView) findViewById(R.id.textView0);

        getFragmentManager()
                .beginTransaction()
                .replace(R.id.leftListFragment, LeftListFragment.newInstance(criterias))
                .commit();

        getFragmentManager()
                .beginTransaction()
                .replace(R.id.rightListFragment, RightListFragment.newInstance(criterias))
                .commit();


        stageName.setText("Porownywanie kryteriow");

        Preferences.Instance().setMatrixType(CRITERIA);

    }

    public void distanceRate() {
        final ArrayList<String> distances = new ArrayList<String>();

        if (mockData) {
            //mocking data
            distances.add("2.2 km");
            distances.add("3.0 km");
            distances.add("3.4 km");
            distances.add("2.3 km");
        } else
            for (int i = 0; i < placesData.length; i++)
                distances.add(placesData[i].getDistanceText());


        getFragmentManager()
                .beginTransaction()
                .replace(R.id.leftListFragment, LeftListFragment.newInstance(distances))
                .commit();

        getFragmentManager()
                .beginTransaction()
                .replace(R.id.rightListFragment, RightListFragment.newInstance(distances))
                .commit();

        stageName.setText("Kryterium odleglosci");

        Preferences.Instance().setMatrixType(DISTANCE);


    }

    private void popularityRate() {

        final ArrayList<String> popularities = new ArrayList<String>();


        if (mockData) {
            popularities.add("34");
            popularities.add("31");
            popularities.add("118");
            popularities.add("105");
        } else
            for (int i = 0; i < placesData.length; i++)
                popularities.add(String.valueOf(placesData[i].getPopularity()));


        getFragmentManager()
                .beginTransaction()
                .replace(R.id.leftListFragment, LeftListFragment.newInstance(popularities))
                .commit();

        getFragmentManager()
                .beginTransaction()
                .replace(R.id.rightListFragment, RightListFragment.newInstance(popularities))
                .commit();


        stageName.setText("Kryterium popularnosci");

        Preferences.Instance().setMatrixType(POPULARITY);

    }

    private void ratingRate() {

        final ArrayList<String> ratings = new ArrayList<String>();

        if (mockData) {
            ratings.add("4.8");
            ratings.add("3.1");
            ratings.add("3.9");
            ratings.add("4.3");
        } else {
            for (int i = 0; i < placesData.length; i++)
                ratings.add(String.valueOf(placesData[i].getRating()));
        }

        getFragmentManager()
                .beginTransaction()
                .replace(R.id.leftListFragment, LeftListFragment.newInstance(ratings))
                .commit();

        getFragmentManager()
                .beginTransaction()
                .replace(R.id.rightListFragment, RightListFragment.newInstance(ratings))
                .commit();


        stageName.setText("Kryterium ocen");

        Preferences.Instance().setMatrixType(RATINGS);

    }
}
