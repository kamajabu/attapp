package com.buczel.attapp.Fragments;

import android.app.ListFragment;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.buczel.attapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Reiz3N on 2016-04-27.
 */
public class RightListFragment extends ListFragment {

    private FragmentConnector connector;
    private FragmentConnector.listType type;

    String[] values, valuesAll;
    ArrayAdapter<String> adapter;

    public static final RightListFragment newInstance(ArrayList<String> values) {
        RightListFragment rlf = new RightListFragment();

        final Bundle bdl = new Bundle(1);
        bdl.putStringArrayList("values", values);
        rlf.setArguments(bdl);

        return rlf;
    }

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        if(getArguments()!=null) {
            List<String> tempList = getArguments().getStringArrayList("values");


            //due to JVM optimizations, using new String[0] is better than new String[list.size()]
            String[] values = tempList.toArray(new String[0]);
            valuesAll = values;

            adapter = new ArrayAdapter<String>(getActivity(), R.layout.rowlayout, R.id.label, values);
            setListAdapter(adapter);

            if (getActivity() instanceof FragmentConnector) {
                connector = (FragmentConnector) getActivity();
                type = FragmentConnector.listType.RIGHT;
            } else {
                throw new ClassCastException(getActivity().toString()
                        + " must implement FragmentConnector");
            }

        }

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        setListShown(true);

    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        updateDetail(position);
    }

    public void updateList(int optionSelected) {

        int numberOfValues = valuesAll.length;
        List<String> tempData = new ArrayList<String>();

        for (int i = 1+optionSelected; i < numberOfValues; i++ ) tempData.add(valuesAll[i]);
        values = tempData.toArray(new String[0]);


        adapter = new ArrayAdapter<String>(getActivity(), R.layout.rowlayout, R.id.label, values);

        setListAdapter(adapter);

    }

    public void updateDetail(int selectedOption) {
        connector.onItemSelected(selectedOption, type);
    }
}
