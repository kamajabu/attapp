package com.buczel.attapp.Fragments;

import android.app.ListFragment;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.buczel.attapp.R;

import java.util.ArrayList;
import java.util.List;

public class LeftListFragment extends ListFragment {


    private FragmentConnector connector;
    private FragmentConnector.listType type;

    public static final LeftListFragment newInstance(ArrayList<String> values) {
        LeftListFragment llf = new LeftListFragment();

        final Bundle bdl = new Bundle(1);
        bdl.putStringArrayList("values", values);
        llf.setArguments(bdl);

        return llf;
    }

    public void onCreate(Bundle bd1) {
        super.onCreate(bd1);

            if(getArguments()!=null) {
                List<String> tempList = getArguments().getStringArrayList("values");


                //due to JVM optimizations, using new String[0] is better than new String[list.size()]
                String[] values = tempList.toArray(new String[0]);

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.rowlayout, R.id.label, values);

                setListAdapter(adapter);




                if (getActivity() instanceof FragmentConnector) {
                    connector = (FragmentConnector) getActivity();
                    type = FragmentConnector.listType.LEFT;
                } else {
                    throw new ClassCastException(getActivity().toString()
                            + " must implement FragmentConnector");
                }
            }


    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        setListShown(true);

    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {

//        Toast.makeText(getActivity(),
//                String.valueOf(position),
//                Toast.LENGTH_SHORT).show();

        updateDetail(position);

    }

    // triggers update of the details fragment
    public void updateDetail(int selectedOption) {
        connector.onItemSelected(selectedOption, type);
    }

}







