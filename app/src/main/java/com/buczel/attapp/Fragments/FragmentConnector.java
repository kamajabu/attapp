package com.buczel.attapp.Fragments;

/**
 * Created by Reiz3N on 2016-05-01.
 */
public interface FragmentConnector {

    public enum listType {
        LEFT, MIDDLE, RIGHT;
    }

    public void onItemSelected(int selectedOption, listType type);


}
