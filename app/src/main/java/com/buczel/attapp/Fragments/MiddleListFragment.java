package com.buczel.attapp.Fragments;

import android.app.ListFragment;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.buczel.attapp.R;
import com.buczel.attapp.Singletons.Preferences;

/**
 * Created by Reiz3N on 2016-04-27.
 */
public class MiddleListFragment extends ListFragment {

    private FragmentConnector connector;
    private FragmentConnector.listType type;

    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        String[] values = new String[] {
                "A jest równoważne z B",
                "A jest bardzo słabo preferowane",
                "A jest słabo preferowane",
                "A jest średnio preferowane",
                "A jest silnie preferowane",
                "A jest więcej niż silnie preferowane",
                "A jest bardzo silnie preferowane",
                "A jest niemal ekstremalnie preferowane",
                "A jest ekstremalnie preferowane"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.rowlayout, R.id.label, values);
        setListAdapter(adapter);

        if (getActivity() instanceof FragmentConnector) {
            connector = (FragmentConnector) getActivity();
            type = FragmentConnector.listType.MIDDLE;
        } else {
            throw new ClassCastException(getActivity().toString()
                    + " must implement FragmentConnector");
        }


    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        updateDetail(position);
    }

    public void updateList(){

        clearMiddle();
        //if current viewed options was selected earlier
        if (Preferences.Instance().getMiddleOption()!=-1)
        getListView().setItemChecked(Preferences.Instance().getMiddleOption(), true);
    }

    public void updateDetail(int selectedOption) {
        connector.onItemSelected(selectedOption, type);
    }

    public void clearMiddle() {
        for (int i = 0; i< getListView().getCount(); i++){
            getListView().setItemChecked(i, false);
        }
    }
}
