package com.buczel.attapp.RetrofitAPI.Details;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Reiz3N on 2016-05-03.
 */
public class _ResponseDetails {
    private static final String TAG = _ResponseDetails.class.getSimpleName();

    private final _ResponseDetails self = this;

    @SerializedName("result")
    private ResultDetails result;

    public _ResponseDetails(ResultDetails result) {
        this.result = result;
    }

    public ResultDetails getResult() {
        return result;
    }

    public void setResult(ResultDetails result) {
        this.result = result;
    }
}

