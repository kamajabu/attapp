package com.buczel.attapp.RetrofitAPI.Details;

import com.buczel.attapp.RetrofitAPI.PlaceLocation.Geometry;

/**
 * Created by Reiz3N on 2016-05-03.
 */

public class ResultDetails {
    private static final String TAG = ResultDetails.class.getSimpleName();

    private final ResultDetails self = this;

    private String place_id;
    private Geometry geometry;
    private String user_ratings_total;
    private String rating;
    private String name;

    public ResultDetails(Geometry geometry, String place_id, String rating, String user_ratings_total, String name) {
        this.geometry = geometry;
        this.place_id = place_id;
        this.rating = rating;
        this.user_ratings_total = user_ratings_total;
        this.name = name;
    }


    public String getPlace_id() {
        return place_id;
    }

    public void setPlace_id(String place_id) {
        this.place_id = place_id;
    }

    public String getUser_ratings_total() {
        return user_ratings_total;
    }

    public void setUser_ratings_total(String user_ratings_total) {
        this.user_ratings_total = user_ratings_total;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
