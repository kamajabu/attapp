package com.buczel.attapp.RetrofitAPI.Search;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Reiz3N on 2016-05-03.
 */
public class _ResponsePlaces {
    private static final String TAG = _ResponsePlaces.class.getSimpleName();
    private final _ResponsePlaces self = this;

    @SerializedName("results")
    private List<ResultSearch> results;

    public _ResponsePlaces(List<ResultSearch> results) {
        this.results = results;
    }

    public List<ResultSearch> getResults() {
        return results;
    }

    public void setResults(List<ResultSearch> results) {
        this.results = results;
    }
}