package com.buczel.attapp.RetrofitAPI.Distance;

import java.util.List;

/**
 * Created by Reiz3N on 2016-05-03.
 */
public class _ResponseDistance {
    private static final String TAG = _ResponseDistance.class.getSimpleName();
    private final _ResponseDistance self = this;

    private List<Row> rows;

    public _ResponseDistance(List<Row> rows) {
        this.rows = rows;
    }

    public List<Row> getRows() {
        return rows;
    }

    public void setRows(List<Row> rows) {
        this.rows = rows;
    }
}