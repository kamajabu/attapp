package com.buczel.attapp.RetrofitAPI.Distance;

/**
 * Created by Reiz3N on 2016-05-03.
 */
public class Element {



    private Duration duration;
    private Distance distance;

    public Element(Distance distance, Duration duration) {
        this.distance = distance;
        this.duration = duration;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public Distance getDistance() {
        return distance;
    }

    public void setDistance(Distance distance) {
        this.distance = distance;
    }


    public class Duration{
        private String value;
        private String text;

        public Duration(String text, String value) {
            this.text = text;
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }
    }

    public class Distance{
        private String value;
        private String text;

        public Distance(String text, String value) {
            this.text = text;
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

    }
}
