package com.buczel.attapp.RetrofitAPI;

import android.content.Context;

import com.buczel.attapp.R;
import com.buczel.attapp.RetrofitAPI.Details._ResponseDetails;
import com.buczel.attapp.RetrofitAPI.Distance._ResponseDistance;
import com.buczel.attapp.RetrofitAPI.Search._ResponsePlaces;
import com.google.android.gms.maps.model.LatLng;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by Reiz3N on 2016-05-03.
 */
public class ApiHelper {
    private static final String TAG = ApiHelper.class.getSimpleName();
    private final ApiHelper self = this;

    private Context mContext;
    private Retrofit retrofit;
    private ApiService service;
    private String webKey = "AIzaSyBlVdpRe_ku98mTpzxH5iST8-Kq9g-EKCw";

    public ApiHelper(Context context) {
        mContext = context;

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        retrofit = new Retrofit.Builder()
                .baseUrl(mContext.getString(R.string.places_api_url))
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        service = retrofit.create(ApiService.class);

    }

    public void requestPlaces(String types, LatLng latLng, int radius, Callback<_ResponsePlaces> callback) {

        Call<_ResponsePlaces> call = service.requestPlaces(
                webKey,
                String.valueOf(latLng.latitude) + "," + String.valueOf(latLng.longitude),
                String.valueOf(radius),
                types);
        call.enqueue(callback);
    }

    public void requestDetails(String placeid, Callback<_ResponseDetails> callback) {
        Call<_ResponseDetails> call = service.requestDetails(
                webKey,
                placeid);
        call.enqueue(callback);
    }

    public void requestDistance(LatLng origins, LatLng destinations, String mode, Callback<_ResponseDistance> callback) {
        Call<_ResponseDistance> call = service.requestDistance(
                webKey,
                String.valueOf(origins.latitude) + "," + String.valueOf(origins.longitude),
                String.valueOf(destinations.latitude) + "," + String.valueOf(destinations.longitude),
                mode);
        call.enqueue(callback);
    }


}
