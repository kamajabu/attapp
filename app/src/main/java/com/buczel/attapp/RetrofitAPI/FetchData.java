package com.buczel.attapp.RetrofitAPI;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import com.buczel.attapp.RetrofitAPI.Details._ResponseDetails;
import com.buczel.attapp.RetrofitAPI.Distance.Element;
import com.buczel.attapp.RetrofitAPI.Distance.Row;
import com.buczel.attapp.RetrofitAPI.Distance._ResponseDistance;
import com.buczel.attapp.RetrofitAPI.PlaceLocation.Location;
import com.buczel.attapp.RetrofitAPI.Search.ResultSearch;
import com.buczel.attapp.RetrofitAPI.Search._ResponsePlaces;
import com.buczel.attapp.Singletons.PlacesData;
import com.buczel.attapp.Singletons.Preferences;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Reiz3N on 2016-05-11.
 */
public class FetchData implements GoogleApiClient.ConnectionCallbacks {

    protected GoogleApiClient mGoogleApiClient;
    LatLng mCurrentLatLng;
    ApiHelper mHelper;
    //it's ugly, but it's easy to implement and works well with asynchronous tasks
    int count, detailIndex, distanceIndex;
    int numberOfData = 4;
    private Context mContext;
    private android.location.Location mLastLocation;
    private Location destinationLoc;
    private int popularity;
    private float rating;
    private Callback<_ResponseDistance> mDistanceResultCallback = new Callback<_ResponseDistance>() {

        @Override
        public void onResponse(Call<_ResponseDistance> call, Response<_ResponseDistance> response) {
            List<Row> rows = response.body().getRows();

            for (Row r : rows) {
                List<Element> elements = r.getElements();
                for (Element e : elements) {
                    PlacesData.Instance().insertDataDistance(
                            distanceIndex,
                            e.getDistance().getText(),
                            Integer.valueOf(e.getDistance().getValue()));
                }
            }
            distanceIndex++;
        }

        @Override
        public void onFailure(Call<_ResponseDistance> call, Throwable t) {
            t.printStackTrace();
        }
    };

    //------------------------------
    private Callback<_ResponseDetails> mDetailsResultCallback = new Callback<_ResponseDetails>() {

        @Override
        public void onResponse(Call<_ResponseDetails> call, retrofit2.Response<_ResponseDetails> response) {

            Random r = new Random();

            popularity = r.nextInt(100);//Integer.valueOf(response.body().getResult().getUser_ratings_total());
            rating = Float.valueOf(response.body().getResult().getRating());
            String name = response.body().getResult().getName();

            PlacesData.Instance().insertDataDetails(detailIndex, popularity, rating, name);

            destinationLoc = response.body().getResult().getGeometry().getLocation();

            LatLng mDestinationLatLng = new LatLng(destinationLoc.getLat(), destinationLoc.getLng());

            mHelper.requestDistance(mCurrentLatLng, mDestinationLatLng, "walking", mDistanceResultCallback);

            detailIndex++;
        }

        @Override
        public void onFailure(Call<_ResponseDetails> call, Throwable t) {
            t.printStackTrace();
        }
    };

    //------------------------------
    private Callback<_ResponsePlaces> mResultCallback = new Callback<_ResponsePlaces>() {
        @Override
        public void onResponse(Call<_ResponsePlaces> call, retrofit2.Response<_ResponsePlaces> response) {

            List<ResultSearch> results = response.body().getResults();
            count = 0;
            for (ResultSearch r : results) {
                if (r.getRating() != null) {
                    if (count < numberOfData)
                        mHelper.requestDetails(r.getPlace_id(), mDetailsResultCallback);
                    count++;
                }
            }
        }

        @Override
        public void onFailure(Call<_ResponsePlaces> call, Throwable t) {
            t.printStackTrace();
        }
    };

    //------------------------------

    public FetchData(Context context) {
        this.mContext = context;
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient
                    .Builder(mContext)
                    .addApi(Places.GEO_DATA_API)
                    .addApi(Places.PLACE_DETECTION_API)
                    .addApi(LocationServices.API)
                    .addApi(ActivityRecognition.API)
                    .addConnectionCallbacks(this)
                    .build();
            mGoogleApiClient.connect();
        }


    }

    @SuppressWarnings("all")
    @Override
    public void onConnected(@Nullable Bundle bundle) {

        mHelper = new ApiHelper(mContext);


        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            mCurrentLatLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            mHelper.requestPlaces(Preferences.Instance().getPlaceType(), mCurrentLatLng, 5000, mResultCallback);
        }


        //new Wyniki().execute();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    public class Wyniki extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                Thread.sleep(1000);
                PlacesData.Data[] wyniki = PlacesData.Instance().getData();
                Log.i("WTF", "AMAMA");
            } catch (InterruptedException e) {
            }

            return null;
        }

    }

}
