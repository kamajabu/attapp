package com.buczel.attapp.RetrofitAPI.Distance;

import java.util.List;

/**
 * Created by Reiz3N on 2016-05-03.
 */
public class Row {
    private static final String TAG = Row.class.getSimpleName();
    private final Row self = this;

    private List<Element> elements;

    public Row(List<Element> elements) {
        this.elements = elements;
    }

    public List<Element> getElements() {
        return elements;
    }

    public void setElements(List<Element> elements) {
        this.elements = elements;
    }
}
