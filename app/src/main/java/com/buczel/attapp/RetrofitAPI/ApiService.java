package com.buczel.attapp.RetrofitAPI;

/**
 * Created by Reiz3N on 2016-05-03.
 */


import com.buczel.attapp.RetrofitAPI.Details._ResponseDetails;
import com.buczel.attapp.RetrofitAPI.Distance._ResponseDistance;
import com.buczel.attapp.RetrofitAPI.Search._ResponsePlaces;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * URL Sample:
 * https://maps.googleapis.com/maps/api/place/search/json
 * ?types=cafe
 * &location=37.787930,-122.4074990
 * &radius=5000
 * &sensor=false
 * &key=YOUR_API_KEY
 */

public interface ApiService {
    @GET("/maps/api/place/search/json")
    Call<_ResponsePlaces> requestPlaces(@Query("key") String key,
                                        @Query("location") String location,
                                        @Query("radius") String radius,
                                        @Query("types") String types
    );

    @GET("/maps/api/place/details/json")
    Call<_ResponseDetails> requestDetails(@Query("key") String key,
                                          @Query("placeid") String placeid
    );

    @GET("/maps/api/distancematrix/json")
    Call<_ResponseDistance> requestDistance(@Query("key") String key,
                                            @Query("origins") String origins,
                                            @Query("destinations") String destinations,
                                            @Query("mode") String mode
    );

}