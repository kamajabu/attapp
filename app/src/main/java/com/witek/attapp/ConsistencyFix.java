package com.witek.attapp;


import Jama.Matrix;

public class ConsistencyFix {

    public double[][] countY(int n) {

        double[][] y = new double[n][n - 1];
        //liczbe wierszow zostawiam taka sama, liczbe kolumn redukuje o 1

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n - 1; j++) {
                if (i > 0 && i - 1 == j) y[i][j] = -i;

                //umieszczamy w macierzu przekatna na ktorej znajduja sie ujemne wartosci numeru wiersza (-1, -2, -3...)
                //pola nad przekatna wypleniamy 1
                //pola pod przekatna wypelniamy -1
                if ((i == 0) || (i == 1 && j > 0) || (i == 2 && j > 1) || (i == 3 && j > 2) || (i == 4 && j > 3))
                    y[i][j] = 1;
            }
        }
        return y;
    }

    /**
     * Naprawa spójności macierzy
     *
     * @param a - macierz do naprawy spójności
     * @return poprawiona macierz
     */
    public double[][] fixMatrix(double[][] a) {
        double[][] b = new double[a.length][a.length];
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a.length; j++)
                //liczymy wartosc logarytmiczna poszczegolnych pol macierzy
                b[i][j] = Math.log(a[i][j]);
        }

        //tworzenie zmiennych pomocniczych
        Matrix B = new Matrix(b);
        int n = a.length;
        int m = a.length;

        double[][] y = countY(n);

        double[][] x = new double[a.length][a.length];
        Matrix X = new Matrix(x);

        double[][] pom = new double[a.length][1];

        double[][] ones1 = new double[1][n];
        double[][] ones2 = new double[n][1];

        //tworzymy wektory jednostkowe o rozmiarze adekwatnym do wprowadzonej macierzy
        for (int i = 0; i < n; i++) {
            ones1[0][i] = 1;
            ones2[i][0] = 1;
        }

        //zamienami wetkor na macierze w JAMA
        Matrix ONES1 = new Matrix(ones1);
        Matrix ONES2 = new Matrix(ones2);


        for (int i = 0; i < n - 1; i++) {

            for (int j = 0; j < y.length; j++) {
                pom[j][0] = y[j][i];
            }
            Matrix HELP = new Matrix(pom);
            Matrix HELPtran = HELP.transpose();

            Matrix PHIY = HELP.times(ONES1).minus(ONES2.times(HELPtran));

            Matrix Btran = B.transpose();
            double HELP3 = Btran.times(PHIY).trace();
            double numberI = Double.valueOf(i + 1 + (i + 1) * (i + 1));
            double factor = HELP3 / numberI;
            X = X.plus(PHIY.times(factor));
        }
        double tempDouble = Double.valueOf(1d / (2.0 * n));
        X = X.times(tempDouble);
        double tempX[][] = X.getArray();

        double[][] fixedMatrix = new double[tempX.length][tempX.length];

        for (int i = 0; i < fixedMatrix.length; i++) {
            for (int j = 0; j < fixedMatrix.length; j++)
                //liczymy eksponente macierzy
                fixedMatrix[i][j] = Math.exp(tempX[i][j]);
        }
        return fixedMatrix;
    }

    /**
     * Wyświetlanie macierzy KWADRATOWEJ w konsoli
     *
     * @param mat - macierz do wyświetlenia
     */
    protected void displayMatrix(double mat[][]) {
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat.length; j++)
                System.out.print(mat[i][j] + "  ");
            System.out.print("\n");
        }
        System.out.print("\n");

    }

    /**
     * Wyswietlenie dowolnych rozmiarów macierzy w konsoli.
     *
     * @param mat - macierz do wyświetlenia
     * @param a   - liczba wierszy macierzy
     * @param b   - liczba kolumn macierzy
     */
    protected void displayMatrix(double mat[][], int a, int b) {
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < b; j++)
                System.out.print(mat[i][j] + "  ");
            System.out.print("\n");
        }
        System.out.print("\n");

    }

}

