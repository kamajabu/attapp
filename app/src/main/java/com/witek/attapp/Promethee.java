package com.witek.attapp;

import java.util.ArrayList;
import java.util.List;

/**
 * http://www.ig.fpms.ac.be/sites/default/files/Cours%20PROMETHEE%20GAIA.pdf
 * od 7 do końca prometee
 * <p/>
 * <p/>
 * na początek z macierzy kryteriów obliczasz wagi
 * następnie obliczasz te pływy w sposób taki
 * że dla pierwszego wariantu sprawdzasz z każdym innym
 * czyli załóżmy
 * Kawiarnia 1
 * kawiarnia 2
 * kawiarnia 3
 * masz takie
 * no i warianty -> odległość ocena cena
 * na podstawie tej funkcji którą przyjąłeś (czyli np. takie same i niżej to 0, a w innych przypadkach to podana wartość podzielona przez 9)
 * obliczasz flow
 * załóżmy kawiarnia 1 jaki ma pozytywny
 * czyli odległość(K1, K2) + ocena(K1, K2) + cena(K1, K2)
 * potem by obliczyć cały pozytywny flow to z innymi liczysz jeszcze
 * czyli dodajesz do tego odległość(K1, K3) + ocena(K1, K3) + cena(K1, K3)
 * i masz pozytywny flow dla K1
 * negatywny liczysz analogicznie zamieniając miejscami czyli (K2, K1) i (K3, K1)
 * dzielisz na liczbę wariantów - 1
 *
 *
 * 1. Obliczenie Wag
 * 2. Obliczenie pływów
 */
public class Promethee {

    private int attributesNumber;
    private int variantsNumber;

    private double[][] weightsM;
    private List<double[][]> mList;

    private double[] attributesWeights;
    private double[][] preferenceDegrees;
    private double[] positiveFlowScores;
    private double[] negativeFlowScores;
    private double[] nettoFlowScores;

    public Promethee(double[][] weightsM, double[][]... matrixes) {
        this.weightsM = weightsM;

        mList = new ArrayList<>();

        for (double[][] matrix : matrixes)
            mList.add(matrix);


        attributesNumber = weightsM.length;
        variantsNumber = matrixes[0].length;

        //1. Obliczenie Wag
        calculateAttributesWeights();

        preferenceDegrees = new double[variantsNumber][variantsNumber];

        positiveFlowScores = new double[variantsNumber];
        negativeFlowScores = new double[variantsNumber];
        nettoFlowScores = new double[variantsNumber];
    }


    private void calculateAttributesWeights() {
        AHP ahp = new AHP();
        this.attributesWeights = ahp.createS(weightsM);
    }

    public double[] calculateBestVariant() {
        //2. Obliczenie plywow
        computeFlowScores();
        return nettoFlowScores;
    }


    private double calculateUnicriterionPreference(double weight) {
        //przyjeta funkcja
        //wagi takie same i niżej to 0, a w innych przypadkach to podana wartość podzielona przez 9
        if (weight <= 1)
            return 0;
        else
            return weight / 9;
    }

    private double calculatePI(int indexOne, int indexTwo) {
        double pi = 0.0;


        for (int i = 0; i < attributesWeights.length; i++) {
            double weight = mList.get(i)[indexOne][indexTwo];
            pi += attributesWeights[i] * calculateUnicriterionPreference(weight);
        }

        return pi;
    }

    private void computeFlowScores() {
        //3. Dla pierwszego wariantu sprawdzasz z każdym innym
        for (int i = 0; i < variantsNumber; i++) {
            for (int j = 0; j < variantsNumber; j++) {

                //Example: odległość(K1, K2) + ocena(K1, K2) + cena(K1, K2)
                preferenceDegrees[i][j] = calculatePI(i, j);

                positiveFlowScores[i] += calculatePI(i, j);
                negativeFlowScores[i] += calculatePI(j, i);
            }

            positiveFlowScores[i] = positiveFlowScores[i] / (variantsNumber - 1);
            negativeFlowScores[i] = negativeFlowScores[i] / (variantsNumber - 1);

            nettoFlowScores[i] = positiveFlowScores[i] - negativeFlowScores[i];
        }
    }

}
