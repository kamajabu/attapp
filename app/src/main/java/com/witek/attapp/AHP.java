package com.witek.attapp;

import com.buczel.attapp.Singletons.Preferences;

import java.util.ArrayList;

/**
 * Created by Witek on 2016-05-15.
 */

public class AHP {

    private final String TAG = "AHP_Algorithm";
    //get matrixes from singleton, comment for tests

    public double critMat[][] = new double[3][3];
    public double distMat[][] = new double[4][4];
    public double popuMat[][] = new double[4][4];
    public double rateMat[][] = new double[4][4];

    /**
     wektor rankingu
     */
    private double R[] = new double[4];
    /**
     * wektor zawierający średnie wierszy macierzy kryteriów
     */
    private double critS[] = new double[3];
    /**
     * wektor zawierający średnie wierszy macierzy kyterium odległości
     */
    private double distS[] = new double[4];
    /**
     * wektor zawierający średnie wierszy macierzy kryterium popularności
     */
    private double popuS[] = new double[4];
    /**
     * wektor zawierający średnie wierszy macierzy kryterium średniej oceny
     */
    private double rateS[] = new double[4];
    /**
     * wspolczynnik RI dla macierzy 3x3
     */
    private double RI3 = 0.58;
    /**
     * wspolczynnik RI dla macierzy 4x4
     */
    private double RI4 = 0.9;

    private boolean makeCos = false, mockData = false;

    public AHP() {
        if (mockData)
            createMock();
        else {
            critMat = Preferences.Instance().getCritMat();
            distMat = Preferences.Instance().getDistMat();
            popuMat = Preferences.Instance().getPopuMat();
            rateMat = Preferences.Instance().getRateMat();
        }
    }

    public double[] findAnswer() {
        R = getR();
        System.out.print("\n Wektor R decyzji:  " + R[0] + " | " + R[1] + " | " + R[2] + " | " + R[3]);

        return R;
    }


    /**
        Funkcja obliczająca wektor rankingu decyzji algorytmem AHP.

        @return Wektor rankingu R
     */
    private double[] getR() {

        critS = createS(critMat);
        distS = createS(distMat);
        popuS = createS(popuMat);
        rateS = createS(rateMat);

        fixIncosistentMatrixes();

        double[] zmienna = new double[distS.length];

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 3; j++) {
                switch (j) {
                    case 0: {
                        zmienna = distS;
                        break;
                    }
                    case 1: {
                        zmienna = popuS;
                        break;
                    }
                    case 2: {
                        zmienna = rateS;
                        break;
                    }
                    default: {
                        break;
                    }

                }
                R[i] += critS[j] * zmienna[i];
            }
        }

        return R;
    }

    /**
     * Fukcja normalizuje dowolną macierz.
     * @param mat - macierz dwuwymiarowa, ktora bedzie znormalizowana
     * @return znormalizowana macierz.
     */
    public double[][] normalizeMatrix(double mat[][]) {
        double suma[] = new double[mat.length];
        double m[][] = new double[mat.length][mat.length];
        int k = 0;

        for (int i = 0; i < mat.length; i++)
            suma[i] = 0;

        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat.length; j++)
                suma[k] += mat[j][i];
            k++;
        }
        k = 0;

        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat.length; j++)
                m[j][i] = mat[j][i] / suma[k];

            k++;
        }
        return m;
    }

    /**
     * Funkcja tworząca wektor zawierający średnie wierszy dowolnej macierzy.
     * @param mat - macierz, na podstawie której tworzony jest wektor
     * @return - wektor zawierający średnie wierszy dowolnej macierzy
     */
    public double[] createS(double mat[][]) {
        double S[] = new double[mat.length];
        double m[][] = normalizeMatrix(mat);
        double suma = 0;

        int k = 0;
        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat.length; j++)
                suma += m[i][j];
            S[k] = suma / mat.length;
            suma = 0;
            k++;
        }
        return S;
    }


    /**
     * Sprawdzanie spójności macierzy.
     * @param mat - macierz, której spójność jest sprawdzana
     * @return true - jeśli macierz jest spójna, false - w przeciwnym przypadku
     */
    public boolean checkConsistency(double mat[][]) {
        double suma[] = new double[mat.length];
        int k = 0;

        for (int i = 0; i < mat.length; i++)
            suma[i] = 0;


        for (int i = 0; i < mat.length; i++) {
            for (int j = 0; j < mat.length; j++)
                suma[k] += mat[j][i];
            k++;
        }

        double cs = 0;
        double S[] = createS(mat);
        for (int i = 0; i < mat.length; i++)
            cs += suma[i] * S[i];

        double factor1 = 1.0 / (mat.length - 1.0);
        double factor2 = cs - mat.length;
        double CI = factor1 * factor2;

        double CR = (mat.length == 3) ? CI / RI3 : CI / RI4;


        if (CR <= 0.1)
            return true;
        else
            return false;
    }

    /**
     * Funkcja określająca niespójne macierze i naprawiająca niespójności.
     */
    private void fixIncosistentMatrixes() {

        ArrayList<String> consistency = new ArrayList<>();
        ConsistencyFix fix = new ConsistencyFix();


        if (!checkConsistency(critMat)) {
            System.out.print("\ncritMat jest niespojna i musi zostac naprawiona.\n");
            System.out.print("\ncritMat przed naprawa:\n");
            fix.displayMatrix(critMat);

            critMat = fix.fixMatrix(critMat);

            System.out.print("CritMat po naprawie:\n");
            fix.displayMatrix(critMat);
        }
        if (!checkConsistency(distMat)) {
            System.out.print("\ndistMat jest niespojna i musi zostac naprawiona.\n");
            System.out.print("\ndistMat przed naprawa:\n");
            fix.displayMatrix(distMat);

            distMat = fix.fixMatrix(distMat);

            System.out.print("distMat po naprawie:\n");
            fix.displayMatrix(distMat);
        }
        if (!checkConsistency(popuMat)) {
            System.out.print("\npopuMat jest niespojna i musi zostac naprawiona.\n");
            System.out.print("\npopuMat przed naprawa:\n");
            fix.displayMatrix(popuMat);

            popuMat = fix.fixMatrix(popuMat);

            System.out.print("popuMat po naprawie:\n");
            fix.displayMatrix(popuMat);
        }
        if (!checkConsistency(rateMat)) {
            System.out.print("\nrateMat jest niespojna i musi zostac naprawiona.\n");
            System.out.print("\nrateMat przed naprawa:\n");
            fix.displayMatrix(rateMat);

            rateMat = fix.fixMatrix(rateMat);

            System.out.print("rateMat po naprawie:\n");
            fix.displayMatrix(rateMat);
        }

    }


    /**
     * Przykladowy zestaw danych do testowania.
     */
    private void createMock() {
        if (makeCos) {
            critMat[0][0] = 1; //----------
            critMat[0][1] = 1;
            critMat[0][2] = 7;

            critMat[1][0] = 1 / critMat[0][1];
            critMat[1][1] = 1; //----------
            critMat[1][2] = 3;

            critMat[2][0] = 1.0 / critMat[0][2];
            critMat[2][1] = 1.0 / critMat[1][2];
            critMat[2][2] = 1; //----------
        } else {
            critMat[0][0] = 1; //----------
            critMat[0][1] = 3;
            critMat[0][2] = 5;

            critMat[1][0] = 1 / critMat[0][1];
            critMat[1][1] = 1; //----------
            critMat[1][2] = 3;

            critMat[2][0] = 1.0 / critMat[0][2];
            critMat[2][1] = 1.0 / critMat[1][2];
            critMat[2][2] = 1; //----------
        }

        distMat[0][0] = 1; //----------
        distMat[0][1] = 7;
        distMat[0][2] = 1;
        distMat[0][3] = 2;

        distMat[1][0] = 1.0 / distMat[0][1];
        distMat[1][1] = 1; //----------
        distMat[1][2] = 2;
        distMat[1][3] = 9;

        distMat[2][0] = 1.0 / distMat[0][2];
        distMat[2][1] = 1.0 / distMat[1][2];
        distMat[2][2] = 1; //----------
        distMat[2][3] = 6;

        distMat[3][0] = 1.0 / distMat[0][3];
        distMat[3][1] = 1.0 / distMat[1][3];
        distMat[3][2] = 1.0 / distMat[2][3];
        distMat[3][3] = 1; //----------

        //--------

        popuMat[0][0] = 1; //----------
        popuMat[0][1] = 3;
        popuMat[0][2] = 7;
        popuMat[0][3] = 2;

        popuMat[1][0] = 1.0 / popuMat[0][1];
        popuMat[1][1] = 1; //----------
        popuMat[1][2] = 4;
        popuMat[1][3] = 8;

        popuMat[2][0] = 1.0 / popuMat[0][2];
        popuMat[2][1] = 1.0 / popuMat[1][2];
        popuMat[2][2] = 1; //----------
        popuMat[2][3] = 6;

        popuMat[3][0] = 1.0 / popuMat[0][3];
        popuMat[3][1] = 1.0 / popuMat[1][3];
        popuMat[3][2] = 1.0 / popuMat[2][3];
        popuMat[3][3] = 1; //----------

        rateMat[0][0] = 1; //----------
        rateMat[0][1] = 2;
        rateMat[0][2] = 4;
        rateMat[0][3] = 6;

        rateMat[1][0] = 1.0 / rateMat[0][1];
        rateMat[1][1] = 1; //----------
        rateMat[1][2] = 4;
        rateMat[1][3] = 4;

        rateMat[2][0] = 1.0 / rateMat[0][2];
        rateMat[2][1] = 1.0 / rateMat[1][2];
        rateMat[2][2] = 1; //----------
        rateMat[2][3] = 7;

        rateMat[3][0] = 1.0 / rateMat[0][3];
        rateMat[3][1] = 1.0 / rateMat[1][3];
        rateMat[3][2] = 1.0 / rateMat[2][3];
        rateMat[3][3] = 7; //----------


    }


}