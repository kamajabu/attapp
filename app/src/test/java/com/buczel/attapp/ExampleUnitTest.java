package com.buczel.attapp;

import com.witek.attapp.AHP;
import com.witek.attapp.ConsistencyFix;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ExampleUnitTest {

    private ConsistencyFix mConsistencyFix;
    private AHP mAHP;
    private double[][] testMat = new double[][]{{1, 2, 4}, {1 / 2d, 1, 8}, {1 / 4d, 1 / 8d, 1}};
    private double[][] consMat = new double[][]{{1, 1, 7}, {1, 1, 3}, {1 / 7d, 1 / 3d, 1}};


    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Before
    public void setUp() {
        mConsistencyFix = new ConsistencyFix();
        mAHP = new AHP();
    }

    @Test
    public void ahp_normalizeMatrix_isCorrect() throws Exception {
        double[][] resultMat = new double[][]
                {{0.5714285714285714, 0.64, 0.3076923076923077},
                        {0.2857142857142857, 0.32, 0.6153846153846154},
                        {0.14285714285714285, 0.04, 0.07692307692307693}};

        assertArrayEquals(mAHP.normalizeMatrix(testMat), resultMat);

    }

    @Test
    public void ahp_createS_isCorrect() throws Exception {
        double[] resultMat = new double[]
                {0.5063736263736264, 0.40703296703296704d, 0.0865934065934066d};

        assertArrayEquals(mAHP.createS(testMat), resultMat, 0);

    }

    @Test
    public void ahp_checkConsistency_isCorrect() throws Exception {

        assertFalse(mAHP.checkConsistency(testMat));

        assertTrue(mAHP.checkConsistency(consMat));
    }

    @Test
    public void conFix_y_isCorrect() throws Exception {
        double[][] resultMat = new double[][]
                        {{1, 1, 1},
                        {-1, 1, 1},
                        {0, -2, 1},
                        {0, 0, -3}};

        double[][] resultMat3 = new double[][]
                        {{1, 1},
                        {-1, 1},
                        {0, -2}};

        assertArrayEquals(mConsistencyFix.countY(4), resultMat);
        assertArrayEquals(mConsistencyFix.countY(3), resultMat3);

    }
}//0.5919649561952441 | 0.2620275344180225 | 0.1006758448060075 | 0.045331664580725906